#4.4/ Using column storage to create 3 new columns named: storage_gb, storage_ssd, storage_hdd.
# #storage_gb stores the volume of the storage. storage_ssd indicates whether a storage is ssd;
#  values of storage_ssd is either 0 or 1 where 1 means the storage device is ssd. storage_hdd
# indicates whether a storage is hdd; values of storage_hdd is either 0 or 1 where 1 means the storage
# device is hdd. Use this column to answer:
#- How many laptop have ssd storage ? How many laptop have hdd storage ?
#- On a same plot, plot a histogram for the volume of ssd storage and a histogram for the volume
# of hdd storage.



######################################################################
import numpy as np
import pandas as pd



############first we convert the columns' name############################
laptops=pd.read_csv('laptops.csv', encoding='latin-1')

#Function clean_label to clean the names of the columns
def clean_label(s):
    s=s.strip()# clean white space in the left and right of str
    s=s.replace("Operating System", "os")
    s=s.replace(" ", "_")
    s = s.replace("(", "")
    s = s.replace(")", "")
    s=s.lower()
    return s

new_label=[clean_label(column) for column in laptops.columns]
laptops.columns=new_label
##########################################################################################

############################################
#4.4.1 Using column storage to create 3 new columns named: storage_gb, storage_ssd, storage_hdd.
# #storage_gb stores the volume of the storage. storage_ssd indicates whether a storage is ssd;
#  values of storage_ssd is either 0 or 1 where 1 means the storage device is ssd. storage_hdd
# indicates whether a storage is hdd; values of storage_hdd is either 0 or 1 where 1 means the storage
# device is hdd.


#manipulating the volume of storage: gb and tb
# get the storage volume
storage_gb_tb=laptops.storage.str.split().apply(lambda x: x[0])

# getting the unit gb or tb
laptops['gb_tb']=storage_gb_tb.str[-2:]

# getting the number part from storage volume
serie_storage_gb_tb=laptops.storage.str.split().apply(lambda x: x[0]).str.slice(0,-2).astype('int')

# Creating a Series that keeps the multipliers for 1 for gb and 1000 for tb
k=laptops.gb_tb.str.replace('GB','1').str.replace('TB','1000').astype('int')

#multiplying two series to get a new column for laptops, that contains the information of the storage volume
laptops['storage_vol_gb']=pd.Series.multiply(serie_storage_gb_tb,k)

print('the storage volume in gb is\n')
print(laptops['storage_vol_gb'])


###  storage_ssd indicates whether a storage is ssd;
#  values of storage_ssd is either 0 or 1 where 1 means the storage device is ssd.
# getting the storage type
storage_kind=laptops.storage.str.split().apply(lambda x: x[-1])

laptops['storage_hdd']=storage_kind.replace('HDD','1').replace(['SSD','Storage','Hybrid'],'0')

laptops['storage_ssd']=storage_kind.replace('SSD','1').replace(['HDD','Storage', 'Hybrid'],'0')

##4.4.2 #- How many laptop have hdd storage ? How many laptop have hdd storage ?
print('the number of laptop have hdd storage is \n')
print(laptops.storage_hdd.value_counts()['1'])

##4.4.3 #- How many laptop have ssd storage ? How many laptop have ssd storage ?
print('the number of laptop have ssd storage is \n')
print(laptops.storage_ssd.value_counts()['1'])

##4.4.4 #- On a same plot, plot a histogram for the volume of ssd storage and a histogram for the volume
# of hdd storage.

import matplotlib.pyplot as plt
plt.style.use("ggplot")
plt.rcParams["figure.figsize"] = [8, 6]

plt.interactive(False)

laptops.storage_vol_gb.hist(bins=30)
plt.show()

x=laptops.loc[laptops.storage_ssd=='1','storage_vol_gb']

y=laptops.loc[laptops.storage_hdd=='1','storage_vol_gb']

x_1=x.value_counts()

y_1=y.value_counts()

plt.hist([x_1, y_1], bins=20, label=['ssd', 'hdd'])
plt.legend(loc='upper right')
plt.show()

##################################################################